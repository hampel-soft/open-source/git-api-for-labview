# Git API for LabVIEW

A collection of LabVIEW VIs for using the git command line client.


## :bulb: Documentation

* https://git-scm.com/doc
* https://dokuwiki.hampel-soft.com/code/open-source/git-api-for-labview


## :rocket: Installation

### :wrench: LabVIEW 2016

The VIs are maintained in LabVIEW 2016.

### :link: Dependencies

None.

## :bulb: Usage

Tbd.

## SSH key with passphrase

For using a SSH key with a passphrase along with our Git API for LabVIEW, you need to [configure git and ssh-agent](https://dokuwiki.hampel-soft.com/kb/scc/git/ssh#ssh_passphrase_management), so that you don't have to enter the passphrase each time.

## :busts_in_silhouette: Contributing 

Please take a look at the [issue tracker](https://gitlab.com/hampel-soft/open-source/gitlab-api-for-labview/issues) for a list of potential tasks.

Our Dokuwiki holds more information on [how to collaborate](https://dokuwiki.hampel-soft.com/processes/collaboration) on an open-source project, in case you need help with the processes. Please [get in touch with us](office@hampel-soft.com) for any further questions.

##  :beers: Credits

The VIs in this repository are created and provided by HAMPEL SOFTWARE ENGINEERING (HSE, www.hampel-soft.com). 

Contributors:
* Joerg Hampel
* Alexander Elbert

## :page_facing_up: License 

This repository and its contents are licensed under a BSD/MIT like license - see the [LICENSE](LICENSE) file for details
